This is a plain retranscription of the following article:
  Philippe De Groote, Yoad Winter.
  A type-logical account of quantification in event semantics.
  Logic and Engineering of Natural Language Semantics 11,
  Nov 2014, Tokyo, Japan.
  〈hal-01102261〉

This presents a toy grammar that covers the several examples that are under
discussion in the course of the paper. It mainly consists of three parts:
• a set of abstract syntactic structures, specified by means of a higher-
  order signature;
• a surface realization of the abstract structures, specified by means of a
  homomorphic translation of the signature;
• a semantic interpretation of the abstract structures, specified by means
  of another homomorphic translation;
