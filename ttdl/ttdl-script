### This ACGtk script is for demonstrations of Type-Theoretic Dynamic Logic
#   (TTDL), formalized within [ttdl.acg]. The latter, as well as a
#   configuration file [config.json] should come along. Try and run:
#       acg -realize config.json ttdl-script

load d sigStrings.acg ;
load d sigSyntax.acg ;
load d sigSemantics.acg ;
load d lexGenerator.acg ;
load d lexInterface.acg ;
list ;
wait ;

# "Peter eats a red apple." should render
# "∃x.apple(x) ∧ red(x) ∧ eat(j, x)".

Generator Interface realize
    eat (a (red apple)) Peter : S ;

# "Peter does not know that the morning star is the evening star."

# "Peter seeks a unicorn." should render wether
# "try j (λx. ∃y. unicorn y ∧ find x y)" (*de re* reading) or
# "∃y. unicorn y ∧ try j (λx. find x y)" (*de dicto* reading).

# "Every man loves a woman." should render wether
# "∀x.man x ⊃ (∃y.woman y ∧ love x y)" or
# "∃y.woman y ∧ (∀x.man x ∧ love x y)".

Generator Interface realize
    love (a woman) (every man) : S ;

# "This red car is a Ferrari."

# "This skillful surgeon is Mary."

# "A man entered the room. He switched on the light."

Generator Interface realize
    (enter (the room) (a man)) + (switch_on (the light) he) : S ;

# "I am the only one in this group who dares to say that I am wrong."

# "A wolf might come in. It would eat you first."

# "Peter does not have a car. He would not know where to park it."

Generator Interface realize
    not (have (a car)) Peter : S ;

# "Either there is no bathroom in this apartment or it is in a funny place."

# "The man who gives his paycheck to his wife is wiser than the man who gives it to his mistress."

# "Peter loves Mary. He smiles at her."

Generator Interface realize
    (love Mary Peter) + (smile_at her he) : S ;

# "Every farmer who owns a donkey beats it." should render
# λeφ.(∀x. farmer x ⊃ (∀y. donkey y ⊃ (own x y ⊃ beat x y))) ∧ φe

Generator Interface realize
    beat it (every (who (own (a donkey)) farmer)) : S ;

Generator Interface realize
    own (a donkey) (a farmer) : S ;

Generator Interface realize
    beat it he : S ;
