Naming convention:
+ Signatures and lexicons should be `CamelCased`
+ Types and constants should be `snake_cased`
+ This convention should be ignored when an established name is available (i.e. `NP` as the noun phrases' type rather than `noun_phrase`)
+ Dedicate one file `sigFooBar.acg` per signature `FooBar` and one file `lexBarFoo.acg` per lexicon `BarFoo`

How to be sure parsing is available?
+ Understand ACGtk only proving parsing for lexicons up to 2nd order
+ 2nd order only depends on the abstract signature
+ Stick to 2nd order by: 
    - only *declaring* atomic types in the abstract signature
    - never *defining* complex types in the abstract signature
    - typing abstract constants without parenthesis
+ Or (trickier), never put a complex type as a domain of an abstract constant
