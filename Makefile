ACGC=acgc

.PHONY: default clean all archive

all: default clean

default: strings logics drt quant-event pt-grammar

##############################################################################

strings: sigEnStr.acgo sigFrStr.acgo

logics: sigFOL.acgo

##############################################################################

quant-event: sigAbstractSyntax.acgo lexSemanticInterpretation.acgo lexSurfaceRealization.acgo

lexSemanticInterpretation.acgo: quant-event/lexSemanticInterpretation.acg sigAbstractSyntax.acgo sigFOL.acgo

lexSurfaceRealization.acgo: quant-event/lexSurfaceRealization.acg sigAbstractSyntax.acgo sigEnStr.acgo

##############################################################################

drt: sigPSG.acgo lexPSG.acgo lexDRT.acgo

lexPSG.acgo: drt/lexPSG.acg sigPSG.acgo sigEnStr.acgo

lexDRT.acgo: drt/lexDRT.acg sigPSG.acgo sigFOL.acgo

##############################################################################

pt-grammar: sigPT.acgo lexPT.acgo

lexPT.acgo: pt-grammar/lexPT.acg sigPT.acgo sigEnStr.acgo

##############################################################################

%.acgo:*/%.acg
	$(ACGC) $(filter-out $<,$^) $<

clean:
	-rm sig*.acgo

mrproper:
	-rm *.acgo *.svg
