Welcome to ARA 🦜
==

ARA relies on ACGtk and gathers theories implemented in Abstract Categorial Grammars:
- [Philippe De Groote, Yoad Winter. A type-logical account of quantification in event semantics. Logic and Engineering of Natural Language Semantics 11, Nov 2014, Tokyo, Japan. 〈hal-01102261〉](quant-event)
- [Kamp, Hans, and Uwe Reyle. From discourse to logic: Introduction to modeltheoretic semantics of natural language, formal logic and discourse representation theory. Vol. 42. Springer Science & Business Media, 2013.](drt)
- [The PT grammar extracted from the PTB by Charniak](pt-grammar)

They all are grounded in:
- [Signatures of strings](string)
- [Signatures of logics](logics)

Compile ARA, run `make`. This should add `lexFoo.acgo` files to the repo.

Then try and run `acg -realize realize-engine.json example-script`!
